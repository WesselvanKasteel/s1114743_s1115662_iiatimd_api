<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Participant extends Model
{

    // The attributes that are mass assignable.
    protected $fillable = ['id'];

    // Many tot many relatie met events
    public function events() {
        return $this->belongsToMany(Event::class);
    }
}
