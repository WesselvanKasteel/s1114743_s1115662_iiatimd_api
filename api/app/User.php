<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use Notifiable, HasApiTokens;


    // The attributes that are mass assignable.
    protected $fillable = ['first_name', 'last_name', 'age', 'email', 'password',];

    // The attributes that should be hidden for arrays.
    protected $hidden = ['password', 'remember_token',];


    // The attributes that should be cast to native types.
    protected $casts = ['email_verified_at' => 'datetime',];

    // Many tot many relatie met events
    public function events() {
        return $this->belongsToMany(Event::class);
    }
}
