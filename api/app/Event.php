<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    
    // The attributes that are mass assignable.
    protected $fillable = ['event_name', 'type', 'people_limit', 'description', 'date'];

    // Many tot many relatie met users
    public function users() {
        return $this->belongsToMany(User::class);
    }

    // Many tot many relatie met participants
    public function participants() {
        return $this->belongsToMany(Participant::class);
    }
}
