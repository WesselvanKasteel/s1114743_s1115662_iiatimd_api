<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Event;
use App\User;
use App\Participant;

class EventController extends Controller
{
    public function create(Request $request) {
    
        // request data valideren
        $request->validate([
            'event_name' => 'required|string',
            'type' => 'required|string',
            'people_limit' => 'required|integer',
            'description' => 'required|string',
            'date' => 'required|date',
        ]);

        // checken of de user oud genoeg is om een event aan te maken
        if ($request->user()->age >= 18) {
            // nieuwe event aanmaken
            $event = new Event([
                'event_name' => $request->event_name,
                'type' => $request->type,
                'people_limit' => $request->people_limit,
                'description' => $request->description,
                'date' => $request->date
            ]);
            
            // nieuw event opslaan in de database
            $event->save();

            // het id van de user opvragen en opslaan in variabele
            $user_id = $request->user()->id;
            $user_id = User::get('id')->where('id', '=', $user_id)->first();

            // het id van de net aangemaakte event opvragen en opslaan in variabele
            $event_id = Event::max('id');

            $event_object = Event::latest()->first();

            // het id van de user koppelen aan het net aangemaakte event id
            $user_id->events()->attach($event_id);

            // het id van de participant koppelen aan het net aangemaakte event
            $event_object->participants()->attach($user_id);

            // bericht terug sturen naar de front-end met de melding dat het aanmaken van een nieuw event is gelukt
            return response()->json([
                'message' => 'Successfully created event!'
            ], 201);
        }

        else {
            // bericht terug sturen naar de front-end met de melding dat de user niet oud genoeg is om een event aan te maken
            return response()->json([
                'message' => 'user is not old enough to create an event'
            ], 201);
        }
        

    }

    public function index(Request $request) {

        $events = Event::get(array('event_name', 'description', 'people_limit', 'date', 'type', 'id'));

        return $events;
    }

    public function join(Request $request) {
        // het id van de user opvragen en opslaan in variabele
        $user_id = $request->user()->id;

        // het id van event opvragen en opslaan in variabele
        $event = Event::where("event_name", "=", $request->name)->first();
    
        // het id van de participant koppelen aan het net aangemaakte event
        $event->participants()->attach($user_id);

        // bericht terug sturen naar de front-end met de melding dat het gelukt is een event te joinen
        return response()->json([

            // hier het event object terug sturen om vervolgens op te slaan in de room db
            'event' => $event,
            
        ], 201);
    }
}
