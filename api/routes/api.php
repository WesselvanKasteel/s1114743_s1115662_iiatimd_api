<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

// --------------------------------------------------------------------------
// API Routes
// --------------------------------------------------------------------------

Route::group([
    'prefix' => 'auth'
], function () {
    Route::post('login', 'AuthController@login');
    Route::post('signup', 'AuthController@signup');

    Route::get('events', 'EventController@index');
  
    Route::group([
      'middleware' => 'auth:api'
    ], function() {
        Route::get('logout', 'AuthController@logout');
        Route::get('user', 'AuthController@user');

        Route::post('join', 'EventController@join');

        Route::post('create', 'EventController@create'); //in de header de bearer token mee sturen voor authenticatie
    });
});

